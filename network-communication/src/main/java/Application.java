import java.util.Arrays;
import java.util.List;

public class Application {
    private static final String WORKER_1 = "http://localhost:8081/task";
    private static final String WORKER_2 = "http://localhost:8082/task";

    public static void main(String[] args) {
        Aggregator aggregator = new Aggregator();
        String task = "10,200";
        String task2 = "123456789,100000000000,7899434888895";

        List<String> results = aggregator.sendTaskToWorkers(Arrays.asList(WORKER_1, WORKER_2),
                Arrays.asList(task, task2));

        for (String result : results) {
            System.out.println(result);
        }
    }

}
