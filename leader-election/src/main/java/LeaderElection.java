import org.apache.zookeeper.*;
import org.apache.zookeeper.data.Stat;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class LeaderElection implements Watcher {

    private static final String ELECTION_NAMESPACE = "/election";
    private ZooKeeper zooKeeper;
    private String currentZNodeName;

    public LeaderElection(ZooKeeper zooKeeper) {
        this.zooKeeper = zooKeeper;
    }

    public void volunteerForLeadership() throws InterruptedException, KeeperException {
        String zNodePrefix = ELECTION_NAMESPACE + "/c_";
        String zNodeFullPath = zooKeeper.create(zNodePrefix, new byte[]{}, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL_SEQUENTIAL);

        System.out.println(zNodeFullPath);
        currentZNodeName = zNodeFullPath.replace(ELECTION_NAMESPACE + "/", "");
    }

    public void reElectLeader() throws InterruptedException, KeeperException {
        // The predecessor is the previous node in the chain, which is the one that the current node will
        // watch for disconnection
        Stat predecessorStat = null;
        String predecessorZNodeName = "";
        boolean isLeader = false;

        // We wrap the following block of code into a while loop, in order to handle race conditions,
        // so that when we exit the block, our current node has successfully found a predecessor, or
        // became the leader
        while (Objects.isNull(predecessorStat) && !isLeader) {
            List<String> zNodes = zooKeeper.getChildren(ELECTION_NAMESPACE, false);
            if (zNodes.isEmpty()) {
                return;
            }

            // Sort the nodes in lexicographical order. The node on the top will be the leader.
            Collections.sort(zNodes);
            String smallestZNode = zNodes.get(0);

            if (smallestZNode.equals(currentZNodeName)) {
                System.out.println("I am the leader!");
                isLeader = true;
            } else {
                System.out.println("I am not the leader, " + smallestZNode + " is the leader...");
                int predecessorIndex = Collections.binarySearch(zNodes, currentZNodeName) - 1;
                predecessorZNodeName = zNodes.get(predecessorIndex);
                // We add a watcher to the predecessor, in order to be notified when the node is disconnected.
                predecessorStat = zooKeeper.exists(ELECTION_NAMESPACE + "/"  + predecessorZNodeName, this);
                System.out.println("Watching node " + predecessorZNodeName);
            }
        }

        System.out.println();
    }

    @Override
    public void process(WatchedEvent watchedEvent) {
        switch (watchedEvent.getType()) {
            case NodeDeleted: {
                try {
                    reElectLeader();
                } catch (InterruptedException | KeeperException e) {
                }
            }
        }
    }
}
